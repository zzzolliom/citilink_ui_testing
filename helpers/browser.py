from selenium.webdriver.chromium.webdriver import ChromiumDriver
from helpers.element import Element,Elements

class Browser:
    """Создает экземпляр класса Element, который, в свою очередь является экземплром класса WebElement"""
    browser: ChromiumDriver

    def __init__(self, browser):
        self.browser = browser


    def element(self, locator) :
        return Element(self.browser, locator)

    def elements(self, locator):
        return Elements(self.browser, locator)

    def object_should_be_equal(self, first_obj, second_obj):
        assert first_obj == second_obj, f"{first_obj} != {second_obj}"