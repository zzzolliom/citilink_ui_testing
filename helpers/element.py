from typing import Tuple

from selenium.common import TimeoutException
from selenium.webdriver.common import by
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.remote.webdriver import WebElement
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.webdriver import ChromiumDriver


class Element:


    def __init__(self,browser: ChromiumDriver, lockator: Tuple[str, str]):
        self.__browser = browser
        self.strategy, self.path = lockator
        self._wait = WebDriverWait(self.__browser, timeout=10)
        self.__element = self.find_element_()
        self.action = ActionChains(self.__browser)

    def find_element_(self) -> WebElement:
        return self._wait.until(EC.presence_of_element_located((By.XPATH, self.path)))

    def click(self):
        self.__element.click()
        return self


    def fill(self, value):
        self.__element.clear()
        self.__element.send_keys()
        return self

    def clear(self):
        self.__element.clear()
        return self

    def should_be_visible(self):
        try:
            WebDriverWait(self.__browser, 10).until(
                EC.visibility_of_element_located((By.XPATH, self.path)))
        except TimeoutException:
                pass

    @property
    def text(self):
        return self.__element.text

    @property
    def move_to_element(self):
        self.action.move_to_element(self.__element).perform()
        return self

    def should_have_text(self, text):
        assert self.__element.text == text
        return self


class Elements:
    def __init__(self, browser: ChromiumDriver, locator: Tuple[str,str]):
        self.__browser = browser
        self.strategy, self.path = locator
        self._wait = WebDriverWait(self.__browser,timeout=10)
        self.__elements: list[Element] = self.find_elements()

    def find_elements(self):
        return WebDriverWait(self.__browser, 10).until(EC.presence_of_all_elements_located((By.XPATH, self.path)))

    def texts(self):
        return [element.text for element in self.__elements]

    def __iter__(self):
        pass

    def get_element_by_test(self):
        pass

    def __getitem__(self, item):
        return self.__elements[item]
