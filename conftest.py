import pytest
from selenium import webdriver

@pytest.fixture(scope="function")
def driver():
    browser = webdriver.Chrome()
    browser.maximize_window()
    browser.implicitly_wait(5)
    yield browser
    browser.quit()


