from pages.base_page import BasePage



class BrandPage(BasePage):

    def brand_page_title(self):
        return self.element(("Заголовок страницы с названием бренда", "//h1[@class = 'Heading Heading_level_1 ProductSet__title']"))

    def sunwind_title(self):
        return self.brand_page_title().text
