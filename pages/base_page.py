from selenium.webdriver.chrome.webdriver import ChromiumDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from helpers.browser import Browser


class BasePage(Browser):
    BASE_URL = "https://www.citilink.ru/."

    def __init__(self, driver):
        super().__init__(driver)
        self._driver: ChromiumDriver = driver
        self._header_block = None
        self._catalog_list = None
    @property
    def path(self):
        raise NotImplementedError
    @property
    def header_block(self):
        from pages.header_block import HeaderBlock
        if self._header_block is None:
            self._header_block = HeaderBlock(self._driver)
            return self._header_block

    @property
    def list_of_catalog(self):
        from pages.catalog import CatalogModal
        if self._catalog_list is None:
            self._catalog_list = CatalogModal(self._driver)
            return self._catalog_list
    def open(self):
        self._driver.get(self.BASE_URL + self.path) #get взяли из webdriver
