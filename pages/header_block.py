from .base_page import BasePage
from .catalog import CatalogModal


class HeaderBlock(BasePage):

    def logo(self):
        return self.element(("Логотип в хедере", "(//div[@data-meta-name= 'Logo'])[2]"))


    def catalog_btn(self):
        return self.element(("Кнопка 'Каталог товаров'", '//div[@class="css-gbhtnj eyoh4ac0"]')) #   //a[@href='/catalog/' and contains(@class, 'css-g6bsk4') and @data-meta-name='DesktopHeaderFixed__catalog-menu']

    def catalogs_title(self):
        return self.element(("Заголовок в каталоге","//span[text() = 'Каталог']"))

    def click_on_catalog_btn(self):
        self.catalog_btn().click()
        return CatalogModal(self.browser)


