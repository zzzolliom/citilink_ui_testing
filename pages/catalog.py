from asyncio import sleep

from .base_page import BasePage
from .brand_page import BrandPage
from .catalogs_items import CatalogsItems
from .main_page import MainPage


class CatalogModal(BasePage):

    def list_of_catalogs_directories_left(self):
        return self.elements(("Список разделов каталога", "(//span[@class = 'e19upju70 e106ikdt0 css-1hno645 e1gjr6xo0' ])"))   #если переходить не с главной старницы, то локатор другой //div[@class = 'CatalogMenu__category-items js--CatalogMenu__category-items']

    def element_of_catalog_directories(self):
        return self.element(("Второй элемент в списке каталога", "(//span[@class = 'e19upju70 e106ikdt0 css-1hno645 e1gjr6xo0'  and contains( text(), 'Бытовая техника')])[2]"))


    def catalog_directories_name(self,index) ->str:
        names = self.list_of_catalogs_directories_left().texts()
        return names[index]

    def name_of_catalogs_element(self):
        catalog_element = self.element(("Виды товаров из раздела каталога","(//h4[@class = 'e114sczy0 eml1k9j0 css-ia8do7 e1gjr6xo0'])[2]"))
        return catalog_element.text

    def brand_name_in_catalog_sub_directory_sunwind(self):
         return self.element(("Ссылка на первый бренд SUNWIND","//div[@class='css-nyw4ul egkhm3m0']/*[7]"))

    def compare_catalogs_titles(self):
        sleep(15)
        first_element = self.element_of_catalog_directories()
        sleep(10)
        first_element.move_to_element
        sleep(10)
        first_element_text = first_element.text
        sleep(10)
        name_of_element = self.name_of_catalogs_element()
        sleep(10)
        self.object_should_be_equal(first_element_text,name_of_element)
    def return_to_main_page_from_catalog_click_logo(self):
        self.header_block.logo().click()
        return MainPage(self.browser)

    def redirect_to_category_page(self):
        catalog_element = self.element_of_catalog_directories()
        catalog_element.click()
        return CatalogsItems(self.browser)

    def check_titles_of_category_page(self):
        catalog_name = self.element_of_catalog_directories().text
        category_page = self.redirect_to_category_page()
        page_name = category_page.page_title().text
        self.object_should_be_equal(catalog_name, page_name)

    def redirect_to_brand_page(self):
        catalog_items = self.element_of_catalog_directories()
        catalog_items.move_to_element
        self.brand_name_in_catalog_sub_directory_sunwind().click()
        return BrandPage(self)

    # def check_brand_titles(self):
    #     brand_page = self.redirect_to_brand_page()
    #     title = brand_page.brand_page_title()
    #     print(title.text)
    #     title.should_have_text("Бытовая техника SUNWIND")




