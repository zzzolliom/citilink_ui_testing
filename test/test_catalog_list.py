from test.base_test_suite import TestBaseSuite
from pages.main_page import MainPage

class TestCatalogList(TestBaseSuite):

    def test_should_be_visible_list_of_catalog(self):
        main_page = self.get_page(MainPage)
        catalog_modal = main_page.header_block.click_on_catalog_btn()
        catalog_modal.list_of_catalog.list_of_catalogs_directories_left()


    def test_return_to_main_page_from_catalog_click_logo(self):
        main_page = self.get_page(MainPage)
        catalog_modal = main_page.header_block.click_on_catalog_btn()
        main_page = catalog_modal.return_to_main_page_from_catalog_click_logo()
        main_page.banner

    def test_check_name_of_catalog(self):
        main_page = self.get_page(MainPage)
        catalog_modal = main_page.header_block.click_on_catalog_btn()  #почему не могу вызвать функции из Element? - потому что ф-и не возващали элемент
        catalog_modal.compare_catalogs_titles()   #работает только в режиме отладки


    def test_redirect_to_category_page(self):
        main_page = self.get_page(MainPage)
        catalog_modal = main_page.header_block.click_on_catalog_btn()
        catalog_modal.check_titles_of_category_page()     #работает только в режиме отладки

    def test_redirect_to_brand_page(self):
        main_page = self.get_page(MainPage)
        catalog_modal = main_page.header_block.click_on_catalog_btn()
        catalog_modal.check_brand_titles()


    # def test_get_text(self):
    #     main_page = self.get_page(MainPage)
    #     catalog_modal = main_page.header_block.click_on_catalog_btn()
    #     category = catalog_modal.list_of_catalogs_directories_left()[1]
    #     text = category.text
    #     print(text)














"""переход на главную через клик на лого 
закрыть модалку через крестик 
При наведении курсора (move_to_element)  на элемент из списка категорий  - справа под строкой поиска отображается название категории и группы товаров данной категории 
При клике на элемент из списка категорий - перенаправление на страницу данной категории  + 
При клике на название бренда в списке категорий товаров - перенаправление на страницу бренда + проверка , что есть элемент на странице 
При клике на категорию товаров  - перенаправление на страницу со списком этих товаров 
"""

"""При клике на поиск , если не смотрели товар - отображены "Хиты продаж" , их 3 
При клике на поиск, если смотрели товар - отображен просмотренный товар """
#закрыть модалку
"""проверить наличие категорий в каталоге по названию - тетс что все категории на месте 
поиск проверить  - поиск в ХЕДЕРЕ 
есть популярные категории , проверить чьо есть карточки товаров 
на 27 минуте дослушать 
"""

