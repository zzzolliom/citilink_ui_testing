from pages.base_page import BasePage
from selenium.webdriver.chromium.webdriver import ChromiumDriver
import typing
import pytest
from conftest import driver

from pages.main_page import MainPage

T = typing.TypeVar("T", bound=BasePage)


class TestBaseSuite:
    browser: ChromiumDriver

    @pytest.fixture(autouse=True)
    def prepare(self, driver):
        self.browser: ChromiumDriver = driver

    def get_page(self, page_class: typing.Type[T]) -> T:
        page = page_class(self.browser)
        page.open()
        return page
